# pricedb-go

[Price Database](https://gitlab.com/alensiljak/price-database) implemented in Go(lang).

Price Database maintains an SQLite database of security prices. It utilizes Finance Quote package for downloading of prices.

It is a CLI application.

## Installation

Install directly from this repository using go.
`go install ...`

## Usage

`pricedb <command> <option>`

## Contributing
Feel free to contribute suggestions and pull requests.

## License
GPL-3.0
